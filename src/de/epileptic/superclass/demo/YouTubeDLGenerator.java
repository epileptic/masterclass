package de.epileptic.superclass.demo;

import java.util.List;

import de.epileptic.superclass.core.Superposition;
import de.epileptic.superclass.docreader.DocReader;
import de.epileptic.superclass.docreader.FooReader;
import de.epileptic.superclass.generator.JavaGenerator;
import de.epileptic.superclass.superclass.Member;

public class YouTubeDLGenerator {

	public static void main(String[] args) {
//		Member verbose = new Member("verbose", Type.BOOLEAN);
//		Member ip = new Member("ip", Type.STRING);
//		Member port = new Member("port", Type.INT);
//		Member timeout = new Member("timeout", Type.FLOATING_POINT);
//
//		Superposition sp = new Superposition("mycommand");
//		sp.add(verbose);
//		sp.add(ip);
//		sp.add(port);
//		sp.add(timeout);
//		sp.assemble(new JavaGenerator());

		Superposition sp = new Superposition("youtube-dl");
		DocReader dc = new FooReader();

		List<Member> members = dc.read("--playlist-start NUMBER          Playlist video to start at (default is 1)\n"
				+ "--playlist-end NUMBER            Playlist video to end at (default is last)\n"
				+ "--playlist-items ITEM_SPEC       Playlist video items to download. Specify\n"
				+ "                                 indices of the videos in the playlist\n"
				+ "                                 separated by commas like: \"--playlist-items\n"
				+ "                                 1,2,5,8\" if you want to download videos\n"
				+ "                                 indexed 1, 2, 5, 8 in the playlist. You can\n"
				+ "                                 specify range: \"--playlist-items\n"
				+ "                                 1-3,7,10-13\", it will download the videos\n"
				+ "                                 at index 1, 2, 3, 7, 10, 11, 12 and 13.\n"
				+ "--match-title REGEX              Download only matching titles (regex or\n"
				+ "                                 caseless sub-string)\n"
				+ "--reject-title REGEX             Skip download for matching titles (regex or\n"
				+ "                                 caseless sub-string)\n"
				+ "--max-downloads NUMBER           Abort after downloading NUMBER files\n"
				+ "--min-filesize SIZE              Do not download any videos smaller than\n"
				+ "                                 SIZE (e.g. 50k or 44.6m)\n"
				+ "--max-filesize SIZE              Do not download any videos larger than SIZE\n"
				+ "                                 (e.g. 50k or 44.6m)\n"
				+ "--date DATE                      Download only videos uploaded in this date\n"
				+ "--datebefore DATE                Download only videos uploaded on or before\n"
				+ "                                 this date (i.e. inclusive)\n"
				+ "--dateafter DATE                 Download only videos uploaded on or after\n"
				+ "                                 this date (i.e. inclusive)\n"
				+ "--min-views COUNT                Do not download any videos with less than\n"
				+ "                                 COUNT views\n"
				+ "--max-views COUNT                Do not download any videos with more than\n"
				+ "                                 COUNT views\n"
				+ "--match-filter FILTER            Generic video filter. Specify any key (see\n"
				+ "                                 the \"OUTPUT TEMPLATE\" for a list of\n"
				+ "                                 available keys) to match if the key is\n"
				+ "                                 present, !key to check if the key is not\n"
				+ "                                 present, key > NUMBER (like \"comment_count\n"
				+ "                                 > 12\", also works with >=, <, <=, !=, =) to\n"
				+ "                                 compare against a number, key = 'LITERAL'\n"
				+ "                                 (like \"uploader = 'Mike Smith'\", also works\n"
				+ "                                 with !=) to match against a string literal\n"
				+ "                                 and & to require multiple matches. Values\n"
				+ "                                 which are not known are excluded unless you\n"
				+ "                                 put a question mark (?) after the operator.\n"
				+ "                                 For example, to only match videos that have\n"
				+ "                                 been liked more than 100 times and disliked\n"
				+ "                                 less than 50 times (or the dislike\n"
				+ "                                 functionality is not available at the given\n"
				+ "                                 service), but who also have a description,\n"
				+ "                                 use --match-filter \"like_count > 100 &\n"
				+ "                                 dislike_count <? 50 & description\" .\n"
				+ "--no-playlist                    Download only the video, if the URL refers\n"
				+ "                                 to a video and a playlist.\n"
				+ "--yes-playlist                   Download the playlist, if the URL refers to\n"
				+ "                                 a video and a playlist.\n"
				+ "--age-limit YEARS                Download only videos suitable for the given\n"
				+ "                                 age\n"
				+ "--download-archive FILE          Download only videos not listed in the\n"
				+ "                                 archive file. Record the IDs of all\n"
				+ "                                 downloaded videos in it.\n"
				+ "--include-ads                    Download advertisements as well\n"
				+ "                                 (experimental)");
		sp.addAll(members);
		sp.assemble(new JavaGenerator());

		System.out.println(sp);
	}
}
