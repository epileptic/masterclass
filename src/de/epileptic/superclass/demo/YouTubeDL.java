package de.epileptic.superclass.demo;

class JWyoutubeDl {
	private String playlistStart;
	private String playlistEnd;
	private String playlistItems;
	private String matchTitle;
	private String rejectTitle;
	private String maxDownloads;
	private String minFilesize;
	private String maxFilesize;
	private String date;
	private String datebefore;
	private String dateafter;
	private String minViews;
	private String maxViews;
	private String matchFilter;
	private boolean noPlaylist;
	private boolean yesPlaylist;
	private String ageLimit;
	private String downloadArchive;
	private boolean includeAds;

	public String getPlaylistStart() {
		return playlistStart;
	}

	public void setPlaylistStart(String playlistStart) {
		this.playlistStart = playlistStart;
	}

	public String getPlaylistEnd() {
		return playlistEnd;
	}

	public void setPlaylistEnd(String playlistEnd) {
		this.playlistEnd = playlistEnd;
	}

	public String getPlaylistItems() {
		return playlistItems;
	}

	public void setPlaylistItems(String playlistItems) {
		this.playlistItems = playlistItems;
	}

	public String getMatchTitle() {
		return matchTitle;
	}

	public void setMatchTitle(String matchTitle) {
		this.matchTitle = matchTitle;
	}

	public String getRejectTitle() {
		return rejectTitle;
	}

	public void setRejectTitle(String rejectTitle) {
		this.rejectTitle = rejectTitle;
	}

	public String getMaxDownloads() {
		return maxDownloads;
	}

	public void setMaxDownloads(String maxDownloads) {
		this.maxDownloads = maxDownloads;
	}

	public String getMinFilesize() {
		return minFilesize;
	}

	public void setMinFilesize(String minFilesize) {
		this.minFilesize = minFilesize;
	}

	public String getMaxFilesize() {
		return maxFilesize;
	}

	public void setMaxFilesize(String maxFilesize) {
		this.maxFilesize = maxFilesize;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDatebefore() {
		return datebefore;
	}

	public void setDatebefore(String datebefore) {
		this.datebefore = datebefore;
	}

	public String getDateafter() {
		return dateafter;
	}

	public void setDateafter(String dateafter) {
		this.dateafter = dateafter;
	}

	public String getMinViews() {
		return minViews;
	}

	public void setMinViews(String minViews) {
		this.minViews = minViews;
	}

	public String getMaxViews() {
		return maxViews;
	}

	public void setMaxViews(String maxViews) {
		this.maxViews = maxViews;
	}

	public String getMatchFilter() {
		return matchFilter;
	}

	public void setMatchFilter(String matchFilter) {
		this.matchFilter = matchFilter;
	}

	public boolean getNoPlaylist() {
		return noPlaylist;
	}

	public void setNoPlaylist(boolean noPlaylist) {
		this.noPlaylist = noPlaylist;
	}

	public boolean getYesPlaylist() {
		return yesPlaylist;
	}

	public void setYesPlaylist(boolean yesPlaylist) {
		this.yesPlaylist = yesPlaylist;
	}

	public String getAgeLimit() {
		return ageLimit;
	}

	public void setAgeLimit(String ageLimit) {
		this.ageLimit = ageLimit;
	}

	public String getDownloadArchive() {
		return downloadArchive;
	}

	public void setDownloadArchive(String downloadArchive) {
		this.downloadArchive = downloadArchive;
	}

	public boolean getIncludeAds() {
		return includeAds;
	}

	public void setIncludeAds(boolean includeAds) {
		this.includeAds = includeAds;
	}

	public String build() {
		StringBuilder sb = new StringBuilder("youtube-dl ");
		if (playlistStart != null)
			sb.append("--playlist-start " + playlistStart + " ");
		if (playlistEnd != null)
			sb.append("--playlist-end " + playlistEnd + " ");
		if (playlistItems != null)
			sb.append("--playlist-items " + playlistItems + " ");
		if (matchTitle != null)
			sb.append("--match-title " + matchTitle + " ");
		if (rejectTitle != null)
			sb.append("--reject-title " + rejectTitle + " ");
		if (maxDownloads != null)
			sb.append("--max-downloads " + maxDownloads + " ");
		if (minFilesize != null)
			sb.append("--min-filesize " + minFilesize + " ");
		if (maxFilesize != null)
			sb.append("--max-filesize " + maxFilesize + " ");
		if (date != null)
			sb.append("--date " + date + " ");
		if (datebefore != null)
			sb.append("--datebefore " + datebefore + " ");
		if (dateafter != null)
			sb.append("--dateafter " + dateafter + " ");
		if (minViews != null)
			sb.append("--min-views " + minViews + " ");
		if (maxViews != null)
			sb.append("--max-views " + maxViews + " ");
		if (matchFilter != null)
			sb.append("--match-filter " + matchFilter + " ");
		if (noPlaylist)
			sb.append("--no-playlist ");
		if (yesPlaylist)
			sb.append("--yes-playlist ");
		if (ageLimit != null)
			sb.append("--age-limit " + ageLimit + " ");
		if (downloadArchive != null)
			sb.append("--download-archive " + downloadArchive + " ");
		if (includeAds)
			sb.append("--include-ads ");
		return sb.toString();
	}
}
