package de.epileptic.superclass.docreader;

import java.util.List;

import de.epileptic.superclass.superclass.Member;

public interface DocReader {
	public List<Member> read(String doc);
}
