package de.epileptic.superclass.docreader;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import de.epileptic.superclass.superclass.Member;
import de.epileptic.superclass.superclass.enums.Type;

public class FooReader implements DocReader {

	@Override
	public List<Member> read(String doc) {
		Scanner dc = new Scanner(doc);

		ArrayList<Member> collection = new ArrayList<Member>();

		String line;
		while (dc.hasNextLine()) {
			line = dc.nextLine();
			line.replaceAll("\t", "    ");
			if (line.charAt(0) != ' ') {
				char[] lc = line.toCharArray();

				StringBuilder name = new StringBuilder();
				Type t = Type.BOOLEAN;

				boolean lastSpace = false;
				char ic;
				for (int i = 2; i < lc.length; i++) {
					ic = lc[i];
					if (ic != ' ') {
						if (lastSpace) {
							t = Type.STRING;
							break;
						}
						name.append(ic);
						lastSpace = false;

					} else {
						if (lastSpace)
							break;
						else
							lastSpace = true;
					}
				}
				collection.add(new Member(name.toString(), t));
			}
		}

		dc.close();
		return collection;
	}

}
