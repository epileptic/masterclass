package de.epileptic.superclass.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import de.epileptic.superclass.generator.Gen;
import de.epileptic.superclass.superclass.Member;

public class Superposition extends ArrayList<Member> {

	private static final long serialVersionUID = 7531979299606909631L;
	private StringBuilder builder;
	private String className;

	public Superposition(String className) {
		this.className = className;
		builder = new StringBuilder();
	}

	public void assemble(Gen generator) {
		builder.append(generator.generateClassHead(className));
		builder.append(generator.getSpacer());

		for (Member i : this) {
			builder.append(generator.generateMember(i, generator.typeToString(i.getType())));
			builder.append(generator.getSpacer());
		}

		for (Member i : this) {
			builder.append(generator.generateGetta(i, generator.typeToString(i.getType())));
			builder.append(generator.getSpacer());
			builder.append(generator.generateSetta(i, generator.typeToString(i.getType())));
			builder.append(generator.getSpacer());
		}

		builder.append(generator.generateCommandBuilderHeader());
		for (Member i : this) {
			builder.append(generator.generateCommandBuilderPart(i, generator.typeToString(i.getType())));
			builder.append(generator.getSpacer());
		}
		builder.append(generator.generateCommandBuilderTail());

		builder.append(generator.generateClassTail());
		builder.append(generator.getSpacer());
	}

	public void toFile(File f) throws IOException {
		if (!f.isFile()) {
			f.getParentFile().mkdir();
			f.createNewFile();
		}
	}

	@Override
	public String toString() {
		return builder.toString();
	}
}
