package de.epileptic.superclass.superclass.enums;

public enum Type {
	INT, STRING, FLOATING_POINT, BOOLEAN
}
