package de.epileptic.superclass.superclass;

import de.epileptic.superclass.superclass.enums.Type;

public class Member {

	private String name;
	private Type type;

	public Member(String name, Type type) {
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public Type getType() {
		return type;
	}
}
