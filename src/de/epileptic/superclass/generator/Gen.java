package de.epileptic.superclass.generator;

import de.epileptic.superclass.superclass.Member;
import de.epileptic.superclass.superclass.enums.Type;

public interface Gen {
	public String generateMember(Member member, String translatedType);

	public String generateGetta(Member member, String translatedType);

	public String generateSetta(Member member, String translatedType);

	public String generateClassHead(String className);

	public String generateClassTail();

	public String generateCommandBuilderHeader();

	public String generateCommandBuilderPart(Member member, String translatedType);

	public String generateCommandBuilderTail();

	public String getSpacer();

	public String typeToString(Type type);

}
