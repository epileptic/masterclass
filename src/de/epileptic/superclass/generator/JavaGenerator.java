package de.epileptic.superclass.generator;

import de.epileptic.superclass.superclass.Member;
import de.epileptic.superclass.superclass.enums.Type;

public class JavaGenerator implements Gen {

	private String className;

	@Override
	public String generateMember(Member member, String translatedType) {
		StringBuilder sb = new StringBuilder();
		if (member.getType() == Type.FLOATING_POINT || member.getType() == Type.INT)
			sb.append("boolean " + format(member.getName()) + "Set;");

		sb.append("private " + translatedType + " " + format(member.getName()) + ";");

		return sb.toString();
	}

	@Override
	public String generateGetta(Member member, String translatedType) {
		return "public " + translatedType + " get" + capitalize(format(member.getName())) + "(){return "
				+ format(member.getName()) + ";}";
	}

	@Override
	public String generateSetta(Member member, String translatedType) {
		if (member.getType() == Type.FLOATING_POINT || member.getType() == Type.INT)
			return "public void set" + capitalize(format(member.getName())) + "(" + translatedType + " "
					+ format(member.getName()) + "){this." + format(member.getName()) + "=" + format(member.getName())
					+ ";this." + format(member.getName()) + "Set" + "=true;}";
		else
			return "public void set" + capitalize(format(member.getName())) + "(" + translatedType + " "
					+ format(member.getName()) + "){this." + format(member.getName()) + "=" + format(member.getName())
					+ ";}";
	}

	@Override
	public String generateClassHead(String className) {
		this.className = className;
		return "class JW" + format(className) + "{";
	}

	@Override
	public String generateClassTail() {
		return "}";
	}

	@Override
	public String generateCommandBuilderHeader() {
		return "public String build() {\nStringBuilder sb = new StringBuilder(\"" + className + " \");";
	}

	@Override
	public String generateCommandBuilderPart(Member member, String translatedType) {

		StringBuilder part = new StringBuilder();

		switch (member.getType()) {
		case STRING:
			part.append("if(" + format(member.getName()) + "!= null)");
			part.append(generateAppend("\"--" + member.getName() + " \"+" + format(member.getName()) + " + \" \""));
			break;

		case BOOLEAN:
			part.append("if(" + format(member.getName()) + ")");
			part.append(generateAppend("\"--" + member.getName() + " \""));
			break;

		case FLOATING_POINT:
			part.append(part.append(generateNumberBuilderPart(member)));
			break;

		case INT:
			part.append(generateNumberBuilderPart(member));
			break;

		}
		return part.toString();
	}

	@Override
	public String generateCommandBuilderTail() {
		return "return sb.toString();}";
	}

	@Override
	public String getSpacer() {
		return "\n";
	}

	private String generateAppend(String append) {
		StringBuilder sb = new StringBuilder();
		sb.append("sb.append(");
		sb.append(append);
		sb.append(");");
		return sb.toString();
	}

	private String generateNumberBuilderPart(Member member) {
		StringBuilder sb = new StringBuilder();
		sb.append("if(" + format(member.getName()) + "Set)");
		sb.append(generateAppend("\"--" + format(member.getName()) + " \"+" + format(member.getName()) + "+\" \""));
		return sb.toString();
	}

	private String capitalize(String subject) {
		char[] c = subject.toCharArray();
		c[0] = Character.toUpperCase(c[0]);
		return new String(c);
	}

	private String format(String subject) {
		StringBuilder sb = new StringBuilder();
		char[] chars = subject.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (chars[i] == '-') {
				i++;
				sb.append(Character.toUpperCase(chars[i]));
			} else {
				sb.append(chars[i]);
			}
		}

		return sb.toString();
	}

	@Override
	public String typeToString(Type type) {
		String result = null;

		switch (type) {
		case INT:
			result = "int";
			break;

		case STRING:
			result = "String";
			break;

		case FLOATING_POINT:
			result = "double";
			break;

		case BOOLEAN:
			result = "boolean";
			break;
		}

		return result;
	}

}
